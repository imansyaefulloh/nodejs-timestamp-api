const express = require('express');

const app = express();

app.get('/', (req, res) => {
  res.send('Timestamp Microservice works');
});

app.listen(3000, () => {
  console.log('server listening at http://localhost:3000');
});
